import sublime
import sublime_plugin
import webbrowser
import re

#'https?://[a-z0-9\./-]+',
#'(https?|ftp|file)://[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?',
#'((https?|ftp|file):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)'

SETTING_FILE = '%s.sublime-settings' % __name__

settings = sublime.load_settings(SETTING_FILE)


def dbg(text):
    if settings.get('show_debug'):
        print '[OpenSelectedUrl] ' + str(text)


def launch_url(url):
    openmode = settings.get('openmode')
    dbg("Opening url '%s' in mode %s" % (str(url), str(openmode)))
    webbrowser.open(url, openmode)


def scan_for_url(input, launch=False):
    has_urls = False
    patterns = settings.get("patterns")
    for pattern in patterns:
        for result in re.findall(pattern, input, re.IGNORECASE):
            if result == None:
                continue
            has_urls = True
            if launch:
                launch_url(result[0])
    return has_urls


class OpenSelectedUrlCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        for region in self.view.sel():
            if not region.empty():
                # Get the selected text
                s = self.view.substr(region)
                # Scan it for URLs
                scan_for_url(s, True)

    def is_enabled(self):
        for region in self.view.sel():
            if not region.empty():
                # Get the selected text
                s = self.view.substr(region)
                # Scan it for URLs
                return scan_for_url(s, False)
        return False

    def description():
        return "Open selected URLs in Browser"
