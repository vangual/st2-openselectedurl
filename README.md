# OpenSelectedUrl

A [Sublime Text 2](http://www.sublimetext.com/) plugin to quickly open selected a URLs in a Browser.

## Installation

### Package Control (TODO)

Install [Package Control](http://wbond.net/sublime_packages/package_control). OpenSelectedUrl will show up in the package list. This is the recommended installation method.

### Manual installation

Go to the "Packages" directory (`Preferences` / `Browse Packages…`). Then clone this repository:

    git clone https://bitbucket.org/vangual/st2-openselectedurl.git

## Options

The options can be found under:

	`Preferences` -> `Package Setings` -> `OpenSelectedUrl` -> `Settings - Default`
and

	`Preferences` -> `Package Setings` -> `OpenSelectedUrl` -> `Settings - User`

**IMPORTANT**: `Settings - Default` will be overwritten on each update. It is *strongly* recommended that you put copies of modified settings into the `Settings - User` file.

### List of available options

	openmode:	Defines how the URLs are opened with the browser. 
				Either normal (0), as a new window (1) or as a new tab (2).
	patterns:	List of regex patterns that identify valid urls.
	show_debug:	Enables messages only useful to the developers.

## Usage

TODO


## Information

Source: [OpenSelectedUrl sourcecode on Bitbucket](https://bitbucket.org/vangual/st2-openselectedurl/src)

Authors: [Vangual](https://bitbucket.org/vangual/)
